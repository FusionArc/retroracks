const firebaseConfig = {
    apiKey: "AIzaSyCX4dtfLKvsV87Y40fJyjVhKNAiyhu7PbE",
    authDomain: "retro-racks.firebaseapp.com",
    databaseURL: "https://retro-racks.firebaseio.com",
    projectId: "retro-racks",
    storageBucket: "retro-racks.appspot.com",
    messagingSenderId: "319240663918",
    appId: "1:319240663918:web:4952bbcca59225d114c77f",
    measurementId: "G-NSJQSJY6KZ"
};


// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

export { firebase }